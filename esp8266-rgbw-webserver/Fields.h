/*
   ESP8266 + FastLED + IR Remote: https://github.com/jasoncoon/esp8266-fastled-webserver
   Copyright (C) 2016 Jason Coon

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

uint8_t power = 0;
uint8_t brightness = brightnessMap[brightnessIndex];
/*
uint8_t sunrise = 1;
uint8_t sunriseHour = 6;
uint8_t sunriseMinute = 15;
uint8_t sunriseDuration = 30;
*/

//String setPower(String value) {
//  power = value.toInt();
//  if(power < 0) power = 0;
//  else if (power > 1) power = 1;
//  return String(power);
//}

String getPower() {
  return String(power);
}

//String setBrightness(String value) {
//  brightness = value.toInt();
//  if(brightness < 0) brightness = 0;
//  else if (brightness > 255) brightness = 255;
//  return String(brightness);
//}

String getBrightness() {
  return String(brightness);
}

String getSolidColor() {
  return String(solidColor.r) + "," + String(solidColor.g) + "," + String(solidColor.b);
}

String getSunrise() {
  return String(sunrise);
}
String getSunriseHour() {
  return String(sunriseHour);
}
String getSunriseMinute() {
  return String(sunriseMinute);
}
String getSunriseDuration() {
  return String(sunriseDuration);
}

FieldList fields = {
  
  { "power", "Power", BooleanFieldType, 0, 1, getPower },
  { "brightness", "Brightness", NumberFieldType, 1, 255, getBrightness },
  //{ "pattern", "Pattern", SelectFieldType, 0, patternCount, getPattern, getPatterns },
  //{ "palette", "Palette", SelectFieldType, 0, paletteCount, getPalette, getPalettes },
  //{ "speed", "Speed", NumberFieldType, 1, 255, getSpeed },
  //{ "autoplay", "Autoplay", SectionFieldType },
  //{ "autoplay", "Autoplay", BooleanFieldType, 0, 1, getAutoplay },
  //{ "autoplayDuration", "Autoplay Duration", NumberFieldType, 0, 255, getAutoplayDuration },
  { "solidColor", "Solid Color", SectionFieldType },
  { "solidColor", "Color", ColorFieldType, 0, 255, getSolidColor },
  { "sunrise", "Sunrise", SectionFieldType },
  { "sunrise", "Sunrise", BooleanFieldType, 0, 1, getSunrise },
  { "sunriseHour",     "Sunrise End Hour",       NumberFieldType, 0, 23, getSunriseHour },
  { "sunriseMinute",   "Sunrise End Minute",     NumberFieldType, 0, 59, getSunriseMinute },
  { "sunriseDuration", "Sunrise Duration (min)", NumberFieldType, 1, 60, getSunriseDuration },
  //{ "fire", "Fire & Water", SectionFieldType },
  //{ "cooling", "Cooling", NumberFieldType, 0, 255, getCooling },
  //{ "sparking", "Sparking", NumberFieldType, 0, 255, getSparking },
  //{ "twinkles", "Twinkles", SectionFieldType },
  //{ "twinkleSpeed", "Twinkle Speed", NumberFieldType, 0, 8, getTwinkleSpeed },
  //{ "twinkleDensity", "Twinkle Density", NumberFieldType, 0, 8, getTwinkleDensity },
  
};

uint8_t fieldCount = ARRAY_SIZE(fields);
