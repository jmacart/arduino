#include "LEDs.h"

// These are the constants calculated from the specific hardware for CIE LUV colorspace.
// u* and v* are not scaled by luminosity because it's irrelevant to angle calculations.

#define Red_ustar 0.311170122
#define Red_vstar 0.0530073442
#define Green_ustar -0.1429469589
#define Green_vstar 0.1050521141
#define Blue_ustar -0.0277372709
#define Blue_vstar -0.3581532103
#define White_uprime 0.2025316456
#define White_vprime 0.4699367089

// Slopes between different locations in colorspace.
#define RGm -0.1146065014
#define GBm -4.0205414357
#define BRm 1.21319441

// Hue Offset to account for physical red not being at 0 degrees in CIE LUV.
// This is used to make the user input easier.
#define RedBase 9.667447472
//#define RedBase 0.0

// Define the angles each color is nominally at
#define RedAngle 9.6674474725
#define GreenAngle 143.6877311667
//#define BlueAngle 265.5715525645
//#define RedAngle 0.0
//#define GreenAngle 120.0
#define BlueAngle 260.0

// Brightness Scaling
#define RedMax 1.0 //78/78
#define GreenMax 1.0 //125/125
#define BlueMax 1.0 //30/30
#define WhiteMax 1.0 //180/180

#define M_PI 3.14159265358979323846264338327950288


RGBWLamp::RGBWLamp(char redpin, char greenpin, char bluepin, char whitepin, int resolution, float PWMfrequency) :
  _redpin(redpin),
  _greenpin(greenpin),
  _bluepin(bluepin),
  _whitepin(whitepin),
  _resolution(resolution),
  _PWMfrequency(PWMfrequency) {
}

void RGBWLamp::begin(void) {
  pinMode(_redpin, OUTPUT);
  pinMode(_greenpin, OUTPUT);
  pinMode(_bluepin, OUTPUT);
  pinMode(_whitepin, OUTPUT);
  analogWrite(_redpin, 0);
  analogWrite(_greenpin, 0);
  analogWrite(_bluepin, 0);
  analogWrite(_whitepin, 0);
  //analogWriteResolution(_resolution);
}

void RGBWLamp::setColor(HSIColor &color, uint16_t *RGBW) {

  float r, g, b, w;
  float H = color.getHue(); H = fmod(H + RedBase + 360, 360);
  float S = color.getSaturation();
  float I = color.getIntensity();

  float tanH = tan(M_PI * fmod(H, 360) / (float)180); // Get the tangent since we will use it often.
  float ustar, vstar;

  boolean success = false;

  // Check the range to determine which intersection to do.

  if ((H >= RedAngle) && (H < GreenAngle)) {
    // Then we are finding the point between Red and Green with the right hue.
    ustar = (Green_vstar - RGm * Green_ustar) / (tanH - RGm);
    vstar = tanH / (RGm - tanH) * (RGm * Green_ustar - Green_vstar);
    r = S * fabs(ustar - Green_ustar) / fabs(Green_ustar - Red_ustar);    /// ABS IN ARDUINO RETURNS IN! NEED FABS!
    g = S * fabs(ustar - Red_ustar) / fabs(Green_ustar - Red_ustar);
    b = 0;
    w = 1 - S;
    success = true;
    //    Serial.println(Green_ustar - Red_ustar);
  }
  else if ((H >= GreenAngle) && (H < BlueAngle)) {
    // Then we are finding the point between Green and Blue with the right hue.
    ustar = (Blue_vstar - GBm * Blue_ustar) / (tanH - GBm);
    vstar = tanH / (GBm - tanH) * (GBm * Blue_ustar - Blue_vstar);
    r = 0;
    g = S * fabs(ustar - Blue_ustar) / fabs(Green_ustar - Blue_ustar);
    b = S * fabs(ustar - Green_ustar) / fabs(Green_ustar - Blue_ustar);
    w = 1 - S;
    success = true;
    //    Serial.println(Green_ustar - Blue_ustar);
  }
  else if (((H >= BlueAngle) && (H < 360)) || (H < RedAngle)) {
    // Then we are finding the point between Green and Blue with the right hue.
    ustar = (Red_vstar - BRm * Red_ustar) / (tanH - BRm);
    vstar = tanH / (BRm - tanH) * (BRm * Red_ustar - Red_vstar);
    r = S * fabs(ustar - Blue_ustar) / fabs(Red_ustar - Blue_ustar);
    g = 0;
    b = S * fabs(ustar - Red_ustar) / fabs(Red_ustar - Blue_ustar);
    w = 1 - S;
    success = true;
    //    Serial.println(S);
    //    Serial.println(fabs(ustar-Blue_ustar));
    //    Serial.println(fabs(Red_ustar - Blue_ustar));
    //    Serial.println(String(r) + ", " + String(g) + ", " + String(b) + ", " + String(w));
  }
  else {
    // Something weird happened.
    ustar = 0;
    vstar = 0;
    r = 0;
    g = 0;
    b = 0;
    w = 0;
  }

  if (success) {
    /*
      Serial.println("Hue:  " + String(H,3) + ", Sat: " + String(S,3) + ", Int: " + String(I,3));
      Serial.println("Tan:  " + String(tanH,3) + ", (u', v'): (" + String(ustar+White_uprime,3) + ", " + String(vstar+White_vprime,3));
      Serial.println("RGBW: " + String((100*r),3) + "% " + String((100*g),3) + "% " + String((100*b),3) + "% " + String((100*w),3) + "%");
    */

    // Mapping Function from rgbw = [0:1] onto their respective ranges.
    // For standard use, this would be [0:1]->[0:0xFFFF] for instance.

    uint16_t rOut = ((1 << _resolution) - 1) * (r * I * RedMax);
    uint16_t gOut = ((1 << _resolution) - 1) * (g * I * GreenMax);
    uint16_t bOut = ((1 << _resolution) - 1) * (b * I * BlueMax);
    uint16_t wOut = ((1 << _resolution) - 1) * (w * I * WhiteMax);
    Serial.println("RGBW Out: " + String(rOut) + ", " + String(gOut) + ", " + String(bOut) + ", " + String(wOut));

    
      analogWrite(_redpin, rOut);
      analogWrite(_greenpin, gOut);
      analogWrite(_bluepin, bOut);
      analogWrite(_whitepin, wOut);
    

    // NEW PWM
    /*
    RGBW[0] = rOut;
    RGBW[1] = gOut;
    RGBW[2] = bOut;
    RGBW[3] = wOut;
    
      pwm_set_duty(rOut, _redpin);
      pwm_set_duty(gOut, _greenpin);
      pwm_set_duty(bOut, _bluepin);
      pwm_set_duty(wOut, _whitepin);
      pwm_start();
    */
  }

  /*
    std::vector<float> LEDs = _colorspace->Hue2LEDs(color);
    for (int i=0; i<LEDs.size(); i++) {
    Serial.print(LEDs[i]); Serial.print(" ");
    LEDs[i] = _maxvalues[i] * LEDs[i];
    }
    setLEDs(LEDs, _pins);
    //Serial.print("\n  ...set the LEDS.\n");
  */
}



HSIColor::HSIColor(float hue, float saturation, float intensity) {
  setHue(hue);
  setSaturation(saturation);
  setIntensity(intensity);
}

// Default constructor.
HSIColor::HSIColor(void) {
  setHue(0);
  setSaturation(0);
  setIntensity(0);
}

// Copy constructor
HSIColor::HSIColor(const HSIColor& oldColor) {
  float hue = oldColor._hue;
  float sat = oldColor._saturation;
  float intense = oldColor._intensity;
  setHue(hue);
  setSaturation(sat);
  setIntensity(intense);
}

void HSIColor::setHue(float hue) {
  _hue = fmod(hue, 360);
}

void HSIColor::setSaturation(float saturation) {
  _saturation = saturation > 0 ? (saturation < 1 ? saturation : 1) : 0;
}

void HSIColor::setIntensity(float intensity) {
  _intensity = intensity > 0 ? (intensity < 1 ? intensity : 1) : 0;
}

void HSIColor::setHSI(float hue, float saturation, float intensity) {
  setHue(hue);
  setSaturation(saturation);
  setIntensity(intensity);
}

void HSIColor::setRGB(int rIn, int gIn, int bIn) {
  // Convert RGB to HSI
  float R, G, B, H, S, I;
  R = float(rIn) / 255.0;
  G = float(gIn) / 255.0;
  B = float(bIn) / 255.0;
  float r, g, b, w, i, minV = 1.e-6;
  i = R + G + B;
  I = i / 3.0;
  r = R / i; b = B / i; g = G / i;

  if (R == G && G == B) {
    S = 0; H = 0;
  } else {
    w = 0.5 * (R - G + R - B) / sqrt((R - G) * (R - G) + (R - B) * (G - B));
    if (w > 1) w = 1;
    if (w < -1) w = -1;
    H = acos(w);
    //if (H<0)
    if (B > G) H = 2 * 3.1415926 - H;
    if (r <= g && r <= b) S = 1.0 - 3.0 * r;
    if (g <= r && g <= b) S = 1.0 - 3.0 * g;
    if (b <= r && b <= g) S = 1.0 - 3.0 * b;
    H = H * 180.0 / 3.1415926;
  }
  setHue(H);
  setSaturation(S);
  //setIntensity(I);
  /*  
    Serial.print(rIn); Serial.print(", ");
    Serial.print(gIn); Serial.print(", ");
    Serial.print(bIn); Serial.print("... ");
    Serial.print(H); Serial.print(", ");
    Serial.print(S); Serial.print(", ");
    Serial.print(I); Serial.print("\n");
  */
}

float HSIColor::getHue(void) {
  return _hue;
}

float HSIColor::getSaturation(void) {
  return _saturation;
}

float HSIColor::getIntensity(void) {
  return _intensity;
}

void HSIColor::getHSI(float *HSI) {
  HSI[0] = getHue();
  HSI[1] = getSaturation();
  HSI[2] = getIntensity();
}

HSIFader::HSIFader(HSIColor color1, HSIColor color2, uint64_t intime, uint8_t indirection) {
  setFader(color1, color2, intime, indirection);
}


void HSIFader::setFader(HSIColor color1, HSIColor color2, uint64_t intime, uint8_t indirection) {
  _colors1.setHue(color1.getHue());
  _colors1.setSaturation(color1.getSaturation());
  _colors1.setIntensity(color1.getIntensity());
  _colors2.setHue(color2.getHue());
  _colors2.setSaturation(color2.getSaturation());
  _colors2.setIntensity(color2.getIntensity());

  _delaymicros = intime * 1000;
  _startmicros = micros();

  //_direction = indirection;
  _direction = 1;
  //if (_colors1.getHue() < _colors2.getHue()) _direction = 0;
  //if (_colors1.getHue() > _colors2.getHue()) _direction = 1;
  // If the hues match, set to constant hue.
  //if (_colors1.getHue() == _colors2.getHue()) _direction = 2;

  float hue1 = _colors1.getHue();
  float hue2 = _colors2.getHue();
  if ((hue2-hue1)>180.0) {
    _hueDifference = hue2-360.0-hue1;
  } else if ((hue1-hue2)>180.0) {
    _hueDifference = hue2-hue1+360.0;
  } else {
    _hueDifference = hue2 - hue1;
  }
  
}


HSIColor HSIFader::getHSIColor() {
  uint64_t runtime = micros() - _startmicros;
  float hue;
  hue = _colors1.getHue() + float(runtime)/float(_delaymicros)*_hueDifference;
  
  /*
  // If direction is 1, rotate positive.
  if (_direction == 1) {
    hue = (_colors1.getHue() * (1.0 - (float)runtime / (float)_delaymicros) + _colors2.getHue() * (float)runtime / (float)_delaymicros);
  }
  // If direction is 0, rotate negative.
  else if (_direction == 0) {
    hue = (_colors1.getHue() * (1.0 - (float)runtime / (float)_delaymicros) + (_colors2.getHue() - 360.0) * (float)runtime / (float)_delaymicros);
  }
  // If direction is 2, constant hue.
  else if (_direction == 2) {
    hue = _colors1.getHue();
  }
  // Otherwise, somethign weird happened. Just set to red.
  else hue = 0;
  */

  float saturation = (_colors1.getSaturation() * (1.0 - (float)runtime / (float)_delaymicros) + _colors2.getSaturation() * (float)runtime / (float)_delaymicros);
  float intensity = (_colors1.getIntensity() * (1.0 - (float)runtime / _delaymicros) + _colors2.getIntensity() * (float)runtime / _delaymicros);

  //  Serial.println("Inside fader: " + String(_colors1.getIntensity()) + ", " + String(_colors2.getIntensity()) + ", " + String((float)runtime/_delaymicros) + ", " + String(intensity));
  return HSIColor(hue, saturation, intensity);
}

boolean HSIFader::isRunning(void) {
  uint64_t runtime = micros() - _startmicros;
  if (runtime <= _delaymicros) return true;
  else return false;
}

/*
  RandomFader::RandomFader(float period) {
  _periodmicros = period*1000;
  }

  void RandomFader::startRandom(float period) {
  _periodmicros = period*1000;
  _LED1 = random(_LEDs.size());
  _LED2 = random(_LEDs.size());
  while(_LED1 == _LED2) _LED2 = random(_LEDs.size());
  _startmicros = micros();
  }

  void RandomFader::addLED(CIELED LED) {
  _LEDs.push_back(LED);
  }

  std::vector<float> RandomFader::getLEDs(void) {
  std::vector<float> LEDOutputs;
  for (int i=0; i<(_LEDs.size()+_effectLEDs.size()); i++) {
    LEDOutputs.push_back(0);
  }
  long time = micros() - _startmicros;
  if (time > _periodmicros) {
    _LED1 = _LED2;
    _LED2 = random(_LEDs.size());
    while(_LED1 == _LED2) _LED2 = random(_LEDs.size());

    // Handle effect LED state.
    if (_effectLEDs.size() != 0) {
      for (unsigned int i=0; i<_effectLEDs.size(); i++) {
        float diceroll = (float)random(1000)/1000;
        switch (_effect[i]) { // Change state based on current state.
          case 0: // Currently off.
            if (diceroll < _effectprob[i]) _effect[i] = 2;
            else _effect[i] = 0;
            break;
          case 1: // Currently on.
            if (diceroll < _effectprob[i]) _effect[i] = 1;
            else _effect[i] = 3;
            break;
          case 2: // Fading on.
            if (diceroll < _effectprob[i]) _effect[i] = 1;
            else _effect[i] = 3;
            break;
          case 3: // Fading off.
            if (diceroll < _effectprob[i]) _effect[i] = 2;
            else _effect[i] = 0;
            break;
        }
      }
    }

    _startmicros += _periodmicros;
  }
  LEDOutputs[_LED1] = _LEDs[_LED1].getMax()*(1-((float)time/_periodmicros));
  LEDOutputs[_LED2] = _LEDs[_LED2].getMax()*((float)time/_periodmicros);

  if (_effectLEDs.size() != 0) {
    for (unsigned int i=0; i<_effectLEDs.size(); i++) {
      switch (_effect[i]) {
        case 0: // Case where it is off.
          LEDOutputs[i+_LEDs.size()] = 0;
          break;
        case 1: // Case where it is on.
          LEDOutputs[i+_LEDs.size()] = 1;
          break;
        case 2: // Case where it is turning on.
          LEDOutputs[i+_LEDs.size()] = (float)time/_periodmicros;
          break;
        case 3: // Case where it is turning off.
          LEDOutputs[i+_LEDs.size()] = 1-(float)time/_periodmicros;
          break;
      }
    }
  }

  // For debugging, print the actual output values.
  //  Serial.println("Output Values. LED1 is " + String(_LED1) + " LED2 is " + String(_LED2));
  //  for (std::vector<float>::iterator i=LEDOutputs.begin(); i != LEDOutputs.end(); ++i) {
  //    Serial.print(*i, 2);
  //    Serial.print(" ");
  //  }
  //  Serial.println("");
  return LEDOutputs;
  }

  std::vector<int> RandomFader::getPins(void) {
  std::vector<int> pins;
  for (int i=0; i<_LEDs.size(); i++) {
    pins.push_back(_LEDs[i].getPin());
  }
  if (_effectLEDs.size() != 0) {
    for (int i=0; i<_effectLEDs.size(); i++) {
      pins.push_back(_effectLEDs[i].getPin());
    }
  }
  return pins;
  }

  void RandomFader::addEffectLED(CIELED LED, float effectprob) {
  _effectLEDs.push_back(LED);
  _effectprob.push_back(effectprob);
  _effect.push_back(0);
  }
*/

HSICycler::HSICycler(HSIColor color, float time, int dir) :
  _color(color) {
  if (dir == 1) _huestep = 0.36 / (float)time;
  if (dir == 0) _huestep = -0.36 / (float)time;
  _lastmicros = micros();
}

void HSICycler::setCycler(HSIColor color, float time, int dir) {
  _color = color;
  if (dir == 1) _huestep = 0.36 / time;
  if (dir == 0) _huestep = -0.36 / time;
  _lastmicros = micros();
}

HSIColor HSICycler::getHSIColor(void) {
  long time = micros() - _lastmicros;
  _color.setHue(_color.getHue() + (time * _huestep));
  _lastmicros = micros();
  return _color;
}
