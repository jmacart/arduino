/*
   ESP8266 RGBW WebServer
   Jonathan F. MacArt
   4 November 2018


   Modified from ESP8266 FastLED WebServer:
   https://github.com/jasoncoon/esp8266-fastled-webserver
   Copyright (C) 2015-2018 Jason Coon

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

extern "C" {
#include "user_interface.h"
#include "pwm.h"
}

#include <ESP8266WiFi.h>
//#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <FS.h>
#include <EEPROM.h>

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

#include "Field.h"

ESP8266WebServer webServer(80);
ESP8266HTTPUpdateServer httpUpdateServer;

#include "FSBrowser.h"

#define NUM_LEDS      4

#ifndef D1
#define D0 16 // cannot use for interrupts
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14 // this is also SCLK
#define D6 12 // this is also MISO
#define D7 13 // this is also MOSI
#define D8 15 // this is also CS
#define D9 3
#define D10 1
#endif

#define REDPIN D1
#define GREENPIN D2
#define BLUEPIN D5
#define WHITEPIN D3



/*
// *********************** ESP8266 NEW PWM ***********************
// Period of PWM frequency -> default of SDK: 5000 -> * 200ns ^= 1 kHz
#define PWM_PERIOD 1024

// PWM Channels
#define PWM_CHANNELS 4

// PWM setup (choice all pins that you use PWM)
uint32 io_info[PWM_CHANNELS][3] = {
  // MUX, FUNC, PIN
  {PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5,   5}, // D1
  {PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4,   4}, // D2
  {PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0,   0}, // D3
//  {PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2,   2}, // D4
  {PERIPHS_IO_MUX_MTMS_U,  FUNC_GPIO14, 14}, // D5
//  {PERIPHS_IO_MUX_MTDI_U,  FUNC_GPIO12, 12}, // D6
//  {PERIPHS_IO_MUX_MTCK_U,  FUNC_GPIO13, 13}, // D7
//  {PERIPHS_IO_MUX_MTDO_U,  FUNC_GPIO15 ,15}, // D8
                         // D0 - not have PWM :-(
};

// PWM initial duty: all off
uint32 pwm_duty_init[PWM_CHANNELS];
*/


// *********************** TeensyLED Headers ***********************
#include "LEDs.h"
#include <memory>

// Create the physical abstraction for the LED controller.
// R, G, B, W, Resolution, Frequency.
RGBWLamp lamp(REDPIN, GREENPIN, BLUEPIN, WHITEPIN, 10, float(183.106));
//RGBWLamp lamp(0, 1, 3, 2, 10, float(183.106));

// Creates a starting HSI color for carrying HSI mode options.
HSIColor color(0, 1, 0);
HSIColor color1(0, 1, 0);
HSIColor color2(0, 1, 0);

// Creates a freerunning HSI Fader.
HSIFader fader(HSIColor(0, 1, 0), HSIColor(0, 1, 0), 1000, 0);

// ******************** END TeensyLED setup ***********************


const bool apMode = false;

#include "Secrets.h" // this file is intentionally not included in the sketch, so nobody accidentally commits their secret information.
// create a Secrets.h file with the following:

// AP mode password
// const char WiFiAPPSK[] = "your-password";

// Wi-Fi network to connect to (if not in AP mode)
// char* ssid = "your-ssid";
// char* password = "your-password";


const uint8_t brightnessCount = 5;
uint8_t brightnessMap[brightnessCount] = { 16, 32, 64, 128, 255 };
uint8_t brightnessIndex = 0;


// time stuff -------------------------------------------------------------------------------------
bool firstTimeGot = false;
String timeStr = "NO-TIME-SET";
String dateStr = "NO-DATE-SET";
// the following 2 ints are just for diagnostic purposes to see how many times we tried to reach NTP and succeeded (and how far)
int nptGets = 0;
int nptAttempts = 0;
bool alarmSet = true;
bool alarmActive = false;
char alarmHrStr[12]   = "6";
char alarmMinStr[12]  = "15";
char alarmStr[12] = "6:15";
String alarmStr2 = "6:15";

// Needs NTP variables
#include "NTP_tools.h"


///////////////////////////////////////////////////////////////////////


bool fade = false;
unsigned long fadeDelay = 2000; // ms
unsigned long fadeStart;

// Sunrise settings
bool sunriseActive = false;
bool sunrise = false;
uint8_t sunriseHour = 6;
uint8_t sunriseMinute = 15;
uint8_t sunriseDuration = 30;
uint64_t sunriseStartSeconds = 0;
uint64_t sunriseStopSeconds  = 0;

uint8_t currentPaletteIndex = 0;

uint8_t gHue = 0; // rotating "base color" used by many of the patterns

// JFM: Replace FastLED solidColor struct with our own definition
typedef struct {
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
  uint8_t w = 0;
} RGBWColor;

RGBWColor solidColor;

uint16_t RGBW_out[4];


#include "Fields.h"

void setup() {
  WiFi.setSleepMode(WIFI_NONE_SLEEP);

  Serial.begin(115200);
  delay(100);
  Serial.setDebugOutput(true);

  EEPROM.begin(512); // Is this big enough??
  loadSettings();

  delay(1000);

  /*
  // PWM settings
  // Set pins (Important! All Pins must be initialized, the PWM SDK not works without this
  pinMode(D1, OUTPUT);
  pinMode(D2, OUTPUT);
  pinMode(D3, OUTPUT);
  pinMode(D4, OUTPUT);
  pinMode(D5, OUTPUT);
  pinMode(D6, OUTPUT);
  pinMode(D7, OUTPUT);
  pinMode(D8, OUTPUT);

  digitalWrite(D1, LOW);
  digitalWrite(D2, LOW);
  digitalWrite(D3, LOW);
  digitalWrite(D4, LOW);
  digitalWrite(D5, LOW);
  digitalWrite(D6, LOW);
  digitalWrite(D7, LOW);
  digitalWrite(D8, LOW);

  ////// Initialize the PWM
  // Initial duty -> all off
  for (uint8_t channel = 0; channel < PWM_CHANNELS; channel++) {
    pwm_duty_init[channel] = 0;
  }

  // Period
  uint32_t period = PWM_PERIOD;

  // Initialize
  pwm_init(period, pwm_duty_init, PWM_CHANNELS, io_info);

  // Commit
  pwm_start();
  */
  
  // And initialize the lamp so that it is fully functional.
  // NOTE: Deprecated with new PWM library
  lamp.begin();

  Serial.println();
  Serial.print( F("Heap: ") ); Serial.println(system_get_free_heap_size());
  Serial.print( F("Boot Vers: ") ); Serial.println(system_get_boot_version());
  Serial.print( F("CPU: ") ); Serial.println(system_get_cpu_freq());
  Serial.print( F("SDK: ") ); Serial.println(system_get_sdk_version());
  Serial.print( F("Chip ID: ") ); Serial.println(system_get_chip_id());
  Serial.print( F("Flash ID: ") ); Serial.println(spi_flash_get_id());
  Serial.print( F("Flash Size: ") ); Serial.println(ESP.getFlashChipRealSize());
  Serial.print( F("Vcc: ") ); Serial.println(ESP.getVcc());
  Serial.println();

  SPIFFS.begin();
  {
    Serial.println("SPIFFS contents:");

    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      Serial.printf("FS File: %s, size: %s\n", fileName.c_str(), String(fileSize).c_str());
    }
    Serial.printf("\n");
  }

  //disabled due to https://github.com/jasoncoon/esp8266-fastled-webserver/issues/62
  //initializeWiFi();

  if (apMode)
  {
    WiFi.mode(WIFI_AP);

    // Do a little work to get a unique-ish name. Append the
    // last two bytes of the MAC (HEX'd) to "Thing-":
    uint8_t mac[WL_MAC_ADDR_LENGTH];
    WiFi.softAPmacAddress(mac);
    String macID = String(mac[WL_MAC_ADDR_LENGTH - 2], HEX) +
                   String(mac[WL_MAC_ADDR_LENGTH - 1], HEX);
    macID.toUpperCase();
    String AP_NameString = "ESP8266 Thing " + macID;

    char AP_NameChar[AP_NameString.length() + 1];
    memset(AP_NameChar, 0, AP_NameString.length() + 1);

    for (int i = 0; i < AP_NameString.length(); i++)
      AP_NameChar[i] = AP_NameString.charAt(i);

    //WiFi.softAP(AP_NameChar, WiFiAPPSK); // JFM disabled

    Serial.printf("Connect to Wi-Fi access point: %s\n", AP_NameChar);
    Serial.println("and open http://192.168.4.1 in your browser");
  }
  else
  {
    WiFi.mode(WIFI_STA);
    Serial.printf("Connecting to %s\n", ssid);
    if (String(WiFi.SSID()) != String(ssid)) {
      WiFi.begin(ssid, password);
    }
  }

  httpUpdateServer.setup(&webServer);

  webServer.on("/all", HTTP_GET, []() {
    String json = getFieldsJson(fields, fieldCount);
    webServer.send(200, "text/json", json);
  });

  webServer.on("/fieldValue", HTTP_GET, []() {
    String name = webServer.arg("name");
    String value = getFieldValue(name, fields, fieldCount);
    webServer.send(200, "text/json", value);
  });

  webServer.on("/fieldValue", HTTP_POST, []() {
    String name = webServer.arg("name");
    String value = webServer.arg("value");
    String newValue = setFieldValue(name, value, fields, fieldCount);
    webServer.send(200, "text/json", newValue);
  });

  webServer.on("/power", HTTP_POST, []() {
    String value = webServer.arg("value");
    setPower(value.toInt());
    sendInt(power);
  });

  // need to extend for rgbw
  webServer.on("/solidColor", HTTP_POST, []() {
    String r = webServer.arg("r");
    String g = webServer.arg("g");
    String b = webServer.arg("b");
    setSolidColor(r.toInt(), g.toInt(), b.toInt());
    sendString(String(solidColor.r) + "," + String(solidColor.g) + "," + String(solidColor.b));
  });

  webServer.on("/brightness", HTTP_POST, []() {
    String value = webServer.arg("value");
    setBrightness(value.toInt());
    sendInt(brightness);
  });

  // settings for sunrise simulation
  webServer.on("/sunrise", HTTP_POST, []() {
    String value = webServer.arg("value");
    setSunrise(value.toInt());
    sendInt(sunrise);
  });
  webServer.on("/sunriseHour", HTTP_POST, []() {
    String value = webServer.arg("value");
    setSunriseHour(value.toInt());
    sendInt(sunriseHour);
  });
  webServer.on("/sunriseMinute", HTTP_POST, []() {
    String value = webServer.arg("value");
    setSunriseMinute(value.toInt());
    sendInt(sunriseMinute);
  });
  webServer.on("/sunriseDuration", HTTP_POST, []() {
    String value = webServer.arg("value");
    setSunriseDuration(value.toInt());
    sendInt(sunriseDuration);
  });

  //list directory
  webServer.on("/list", HTTP_GET, handleFileList);
  //load editor
  webServer.on("/edit", HTTP_GET, []() {
    if (!handleFileRead("/edit.htm")) webServer.send(404, "text/plain", "FileNotFound");
  });
  //create file
  webServer.on("/edit", HTTP_PUT, handleFileCreate);
  //delete file
  webServer.on("/edit", HTTP_DELETE, handleFileDelete);
  //first callback is called after the request has ended with all parsed arguments
  //second callback handles file uploads at that location
  webServer.on("/edit", HTTP_POST, []() {
    webServer.send(200, "text/plain", "");
  }, handleFileUpload);

  webServer.serveStatic("/", SPIFFS, "/", "max-age=86400");

  webServer.begin();
  Serial.println("HTTP web server started");
}

void sendInt(uint8_t value)
{
  sendString(String(value));
}

void sendString(String value)
{
  webServer.send(200, "text/plain", value);
}

void broadcastInt(String name, uint8_t value)
{
  String json = "{\"name\":\"" + name + "\",\"value\":" + String(value) + "}";
  //  webSocketsServer.broadcastTXT(json);
}

void broadcastString(String name, String value)
{
  String json = "{\"name\":\"" + name + "\",\"value\":\"" + String(value) + "\"}";
  //  webSocketsServer.broadcastTXT(json);
}




enum mode {HSI = 0, Strobe = 1, Fade = 2, Sunrise = 3, Random = 4} mode = Random;

bool initPower = false;
int sunriseStage = 1;
bool sunriseDone = false;

void loop() {
  // sync our internal clock periodically with real time from remote server 
  //    (also gets actual time rather than relative from boot time)
  //
  // NOTE: Check NTP_tools.h to verify that we don't need dateStr (bottom line)
  //
  updateTimeFromServer();
  
  webServer.handleClient();

  if (!initPower) {
    setPower(power);
    initPower = true;
  }

  static bool hasConnected = false;
  //EVERY_N_SECONDS(1) {
  if (WiFi.status() != WL_CONNECTED) {
    hasConnected = false;
  }
  else if (!hasConnected) {
    hasConnected = true;
    Serial.print("Connected! Open http://");
    Serial.print(WiFi.localIP());
    Serial.println(" in your browser");
  }

  // Handle the sunrise timer
  handleTimer();

  // Switch the mode to do something
  switch (mode) {
    case 0:
      handleHSI();
    case 2:
      handleFade();
    case 3:
      handleSunrise();
    case 4:
      // do nothing
      return;
  }

}


/*
void writeLEDS(uint16_t *RGBW) {
  pwm_set_duty(RGBW[0], 0);
  pwm_set_duty(RGBW[1], 1);
  pwm_set_duty(RGBW[2], 3);
  pwm_set_duty(RGBW[3], 2);
  pwm_start();
}
*/

void handleTimer() {
  uint64_t currSecs = hour() * 3600 + minute() * 60 + second();
  
  if (sunrise) {
    if ((sunriseStartSeconds <= currSecs) && (sunriseStopSeconds > currSecs) && !sunriseActive) {
      mode = Sunrise;
      sunriseStage = 1;
      sunriseActive = true;
    }
    else if (((sunriseStartSeconds > currSecs) || (sunriseStopSeconds <= currSecs)) && sunriseActive) {
      mode = Random;
      sunriseActive = false;
      setBrightness(0.0);
    }
  }
}


void handleHSI() {
  lamp.setColor(color, RGBW_out);
  mode = Random;
}


void handleFade() {
  // If the fader is running, update it.
  if (fader.isRunning()) {
    color = fader.getHSIColor();
    //Serial.println("Outside fader: " + String(color.getIntensity()));
    lamp.setColor(color, RGBW_out);
  }
  // If the fader is done, set state back to off.
  else mode = Random;
}


/*
 BUGS: 
   - Sunrise: very low brightness flickering. Just keep at min brightness blue for awhile.
   - Sunrise: green colors?
*/

void handleSunrise() {
  // If the sunrise is running, update it.
  // We use the fader object to do this with a few pre-defined stages
  if (fader.isRunning()) {
    color = fader.getHSIColor();
    lamp.setColor(color, RGBW_out);

  } else if (sunriseStage == 1) {
    // Dark to (230.3, 0.18, 0.15)
    color1.setHSI(250.0, 1.0, 0.00);
    color2.setHSI(250.0, 1.0, 0.004);
    uint8_t fadeDirection = 0; // 1: rotate pos; 0: rotate neg
    uint64_t fadeTime = 60000*sunriseDuration*0.2;
    fader.setFader(color1, color2, fadeTime, fadeDirection);
    sunriseStage++;

  } else if (sunriseStage == 2) {
    // To (219.0, 0.41, 0.30)
    color1 = color2;
    color2.setHSI(260.0, 1.0, 0.004);
    uint8_t fadeDirection = 0; // 1: rotate pos; 0: rotate neg
    uint64_t fadeTime = 60000*sunriseDuration*0.2;
    fader.setFader(color1, color2, fadeTime, fadeDirection);
    sunriseStage++;

  } else if (sunriseStage == 3) {
    // To (349.0, 0.54, 0.5)
    color1 = color2;
    color2.setHSI(265.0, 0.85, 0.01);
    uint8_t fadeDirection = 1; // 1: rotate pos; 0: rotate neg
    uint64_t fadeTime = 60000*sunriseDuration*0.2;
    fader.setFader(color1, color2, fadeTime, fadeDirection);
    sunriseStage++;

  } else if (sunriseStage == 4) {
    // To (10, 0.15, 0.60)
    color1 = color2;
    color2.setHSI(005.0, 0.50, 0.20);
    uint8_t fadeDirection = 1; // 1: rotate pos; 0: rotate neg
    uint64_t fadeTime = 60000*sunriseDuration*0.2;
    fader.setFader(color1, color2, fadeTime, fadeDirection);
    sunriseStage++;

  } else if (sunriseStage == 5) {
    // To (349.0, 0.54, 0.5)
    color1 = color2;
    color2.setHSI(008.0, 0.02, 0.90);
    uint8_t fadeDirection = 1; // 1: rotate pos; 0: rotate neg
    uint64_t fadeTime = 60000*sunriseDuration*0.2;
    fader.setFader(color1, color2, fadeTime, fadeDirection);
    sunriseStage++;

  } else {
    // Sunrise is over
    // TO DO: have this do something smart afterward
    mode = Random;
  }
}



void loadSettings()
{
  brightness = EEPROM.read(0);

  byte w = EEPROM.read(1);
  byte r = EEPROM.read(2);
  byte g = EEPROM.read(3);
  byte b = EEPROM.read(4);

  if (w == 0 && r == 0 && g == 0 && b == 0)
  {
  }
  else
  {
    solidColor.w = int(w) * 1023 / 255;
    solidColor.r = int(r) * 1023 / 255;
    solidColor.g = int(g) * 1023 / 255;
    solidColor.b = int(b) * 1023 / 255;
    color.setRGB(r, g, b); // Set the color object
  }

  power = EEPROM.read(5);

  sunrise = EEPROM.read(6);
  sunriseHour = EEPROM.read(7);
  sunriseMinute = EEPROM.read(8);
  sunriseDuration = EEPROM.read(9);
}

void setPower(uint8_t value)
{
  power = value == 0 ? 0 : 1;

  if (power) {
    color.setIntensity(float(brightness) / 255.0);
  } else {
    color.setIntensity(0.0);
  }
  mode = HSI;

  EEPROM.write(5, power);
  EEPROM.commit();

  broadcastInt("power", power);
}

void setSunrise(uint8_t value)
{
  sunrise = value == 0 ? 0 : 1;

  EEPROM.write(6, sunrise);
  EEPROM.commit();

  broadcastInt("sunrise", sunrise);

  if (sunrise) {
    mode = Sunrise;
    sunriseStage = 1;
  }
}

void setSunriseHour(uint8_t value)
{
  sunriseHour = value;
  sunriseStopSeconds  = sunriseHour*3600 + sunriseMinute*60;
  sunriseStartSeconds = sunriseStopSeconds - sunriseDuration*60;

  EEPROM.write(7, sunriseHour);
  EEPROM.commit();

  broadcastInt("sunriseHour", sunriseHour);
}

void setSunriseMinute(uint8_t value)
{
  sunriseMinute = value;
  sunriseStopSeconds  = sunriseHour*3600 + sunriseMinute*60;
  sunriseStartSeconds = sunriseStopSeconds - sunriseDuration*60;

  EEPROM.write(8, sunriseMinute);
  EEPROM.commit();

  broadcastInt("sunriseMinute", sunriseMinute);
}

void setSunriseDuration(uint8_t value)
{
  sunriseDuration = value;
  sunriseStartSeconds = sunriseStopSeconds - sunriseDuration*60;

  EEPROM.write(9, sunriseDuration);
  EEPROM.commit();

  broadcastInt("sunriseDuration", sunriseDuration);
}


void setSolidColor(uint8_t r, uint8_t g, uint8_t b)
{
  // Set target color
  solidColor.r = r;
  solidColor.g = g;
  solidColor.b = b;
  solidColor.w = solidColor.w;

  /*
  // Convert RGB to HSI
  color.setRGB(r, g, b);
  color.setIntensity(float(brightness)/255.0);
  // Set the mode so we catch it on the next time in the loop
  mode = HSI;
  */

  
  // Fade into new color
  color1 = color;
  color2.setRGB(r, g, b);
  color2.setIntensity(float(brightness)/255.0);
  uint8_t fadeDirection = 0;
  uint64_t fadeTime = 1500;
  fader.setFader(color1, color2, fadeTime, fadeDirection);
  mode = Fade;

  EEPROM.write(2, r);
  EEPROM.write(3, g);
  EEPROM.write(4, b);
  EEPROM.commit();

  broadcastString("color", String(solidColor.r) + "," + String(solidColor.g) + "," + String(solidColor.b));
}



void setBrightness(uint8_t value)
{
  if (value > 255)
    value = 255;
  else if (value < 0) value = 0;

  brightness = value;

  //  this needs to be better (HSV conversion)
  solidColor.w = brightness;

  /*
  color.setIntensity(float(brightness) / 255.0);
  mode = HSI;
*/
  
  color1 = color;
  color2 = color;
  color2.setIntensity(float(brightness)/255.0);
  uint8_t fadeDirection = 0;
  uint64_t fadeTime = 1500;
  fader.setFader(color1, color2, fadeTime, fadeDirection);
  mode = Fade;

  EEPROM.write(0, brightness);
  EEPROM.commit();

  broadcastInt("brightness", brightness);
}






/*


  void strandTest()
  {
  static uint8_t i = 0;

  EVERY_N_SECONDS(1)
  {
    i++;
    if (i >= NUM_LEDS)
      i = 0;
  }

  fill_solid(leds, NUM_LEDS, CRGB::Black);

  leds[i] = solidColor;
  }

  void showSolidColor()
  {
  fill_solid(leds, NUM_LEDS, solidColor);
  }

  // Patterns from FastLED example DemoReel100: https://github.com/FastLED/FastLED/blob/master/examples/DemoReel100/DemoReel100.ino

  void rainbow()
  {
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 255 / NUM_LEDS);
  }

  void rainbowWithGlitter()
  {
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
  }

  void rainbowSolid()
  {
  fill_solid(leds, NUM_LEDS, CHSV(gHue, 255, 255));
  }

  void confetti()
  {
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  // leds[pos] += CHSV( gHue + random8(64), 200, 255);
  //leds[pos] += ColorFromPalette(palettes[currentPaletteIndex], gHue + random8(64));
  }

  void sinelon()
  {
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16(speed, 0, NUM_LEDS);
  static int prevpos = 0;
  CRGB color = ColorFromPalette(palettes[currentPaletteIndex], gHue, 255);
  if ( pos < prevpos ) {
    fill_solid( leds + pos, (prevpos - pos) + 1, color);
  } else {
    fill_solid( leds + prevpos, (pos - prevpos) + 1, color);
  }
  prevpos = pos;
  }

  void bpm()
  {
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t beat = beatsin8( speed, 64, 255);
  CRGBPalette16 palette = palettes[currentPaletteIndex];
  for ( int i = 0; i < NUM_LEDS; i++) {
    leds[i] = ColorFromPalette(palette, gHue + (i * 2), beat - gHue + (i * 10));
  }
  }

  void juggle()
  {
  static uint8_t    numdots =   4; // Number of dots in use.
  static uint8_t   faderate =   2; // How long should the trails be. Very low value = longer trails.
  static uint8_t     hueinc =  255 / numdots - 1; // Incremental change in hue between each dot.
  static uint8_t    thishue =   0; // Starting hue.
  static uint8_t     curhue =   0; // The current hue
  static uint8_t    thissat = 255; // Saturation of the colour.
  static uint8_t thisbright = 255; // How bright should the LED/display be.
  static uint8_t   basebeat =   5; // Higher = faster movement.

  static uint8_t lastSecond =  99;  // Static variable, means it's only defined once. This is our 'debounce' variable.
  uint8_t secondHand = (millis() / 1000) % 30; // IMPORTANT!!! Change '30' to a different value to change duration of the loop.

  if (lastSecond != secondHand) { // Debounce to make sure we're not repeating an assignment.
    lastSecond = secondHand;
    switch (secondHand) {
      case  0: numdots = 1; basebeat = 20; hueinc = 16; faderate = 2; thishue = 0; break; // You can change values here, one at a time , or altogether.
      case 10: numdots = 4; basebeat = 10; hueinc = 16; faderate = 8; thishue = 128; break;
      case 20: numdots = 8; basebeat =  3; hueinc =  0; faderate = 8; thishue = random8(); break; 
      // Only gets called once, and not continuously for the next several seconds. Therefore, no rainbows.
      case 30: break;
    }
  }

  // Several colored dots, weaving in and out of sync with each other
  curhue = thishue; // Reset the hue values.
  fadeToBlackBy(leds, NUM_LEDS, faderate);
  for ( int i = 0; i < numdots; i++) {
    //beat16 is a FastLED 3.1 function
    leds[beatsin16(basebeat + i + numdots, 0, NUM_LEDS)] += CHSV(gHue + curhue, thissat, thisbright);
    curhue += hueinc;
  }
  }

  void fire()
  {
  heatMap(HeatColors_p, true);
  }

  void water()
  {
  heatMap(IceColors_p, false);
  }

  // Pride2015 by Mark Kriegsman: https://gist.github.com/kriegsman/964de772d64c502760e5
  // This function draws rainbows with an ever-changing,
  // widely-varying set of parameters.
  void pride()
  {
  static uint16_t sPseudotime = 0;
  static uint16_t sLastMillis = 0;
  static uint16_t sHue16 = 0;

  uint8_t sat8 = beatsin88( 87, 220, 250);
  uint8_t brightdepth = beatsin88( 341, 96, 224);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 1, 3000);

  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5, 9);
  uint16_t brightnesstheta16 = sPseudotime;

  for ( uint16_t i = 0 ; i < NUM_LEDS; i++) {
    hue16 += hueinc16;
    uint8_t hue8 = hue16 / 256;

    brightnesstheta16  += brightnessthetainc16;
    uint16_t b16 = sin16( brightnesstheta16  ) + 32768;

    uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
    uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
    bri8 += (255 - brightdepth);

    CRGB newcolor = CHSV( hue8, sat8, bri8);

    uint16_t pixelnumber = i;
    pixelnumber = (NUM_LEDS - 1) - pixelnumber;

    nblend( leds[pixelnumber], newcolor, 64);
  }
  }

  void radialPaletteShift()
  {
  for (uint16_t i = 0; i < NUM_LEDS; i++) {
    // leds[i] = ColorFromPalette( gCurrentPalette, gHue + sin8(i*16), brightness);
    leds[i] = ColorFromPalette(gCurrentPalette, i + gHue, 255, LINEARBLEND);
  }
  }

  // based on FastLED example Fire2012WithPalette: https://github.com/FastLED/FastLED/blob/master/examples/Fire2012WithPalette/Fire2012WithPalette.ino
  void heatMap(CRGBPalette16 palette, bool up)
  {
  fill_solid(leds, NUM_LEDS, CRGB::Black);

  // Add entropy to random number generator; we use a lot of it.
  random16_add_entropy(random(256));

  // Array of temperature readings at each simulation cell
  static byte heat[256];

  byte colorindex;

  // Step 1.  Cool down every cell a little
  for ( uint16_t i = 0; i < NUM_LEDS; i++) {
    heat[i] = qsub8( heat[i],  random8(0, ((cooling * 10) / NUM_LEDS) + 2));
  }

  // Step 2.  Heat from each cell drifts 'up' and diffuses a little
  for ( uint16_t k = NUM_LEDS - 1; k >= 2; k--) {
    heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
  }

  // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
  if ( random8() < sparking ) {
    int y = random8(7);
    heat[y] = qadd8( heat[y], random8(160, 255) );
  }

  // Step 4.  Map from heat cells to LED colors
  for ( uint16_t j = 0; j < NUM_LEDS; j++) {
    // Scale the heat value from 0-255 down to 0-240
    // for best results with color palettes.
    colorindex = scale8(heat[j], 190);

    CRGB color = ColorFromPalette(palette, colorindex);

    if (up) {
      leds[j] = color;
    }
    else {
      leds[(NUM_LEDS - 1) - j] = color;
    }
  }
  }

  void addGlitter( uint8_t chanceOfGlitter)
  {
  if ( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
  }

  ///////////////////////////////////////////////////////////////////////

  // Forward declarations of an array of cpt-city gradient palettes, and
  // a count of how many there are.  The actual color palette definitions
  // are at the bottom of this file.
  extern const TProgmemRGBGradientPalettePtr gGradientPalettes[];
  extern const uint8_t gGradientPaletteCount;

  uint8_t beatsaw8( accum88 beats_per_minute, uint8_t lowest = 0, uint8_t highest = 255,
                  uint32_t timebase = 0, uint8_t phase_offset = 0)
  {
  uint8_t beat = beat8( beats_per_minute, timebase);
  uint8_t beatsaw = beat + phase_offset;
  uint8_t rangewidth = highest - lowest;
  uint8_t scaledbeat = scale8( beatsaw, rangewidth);
  uint8_t result = lowest + scaledbeat;
  return result;
  }

  void colorWaves()
  {
  colorwaves( leds, NUM_LEDS, gCurrentPalette);
  }

  // ColorWavesWithPalettes by Mark Kriegsman: https://gist.github.com/kriegsman/8281905786e8b2632aeb
  // This function draws color waves with an ever-changing,
  // widely-varying set of parameters, using a color palette.
  void colorwaves( CRGB* ledarray, uint16_t numleds, CRGBPalette16& palette)
  {
  static uint16_t sPseudotime = 0;
  static uint16_t sLastMillis = 0;
  static uint16_t sHue16 = 0;

  // uint8_t sat8 = beatsin88( 87, 220, 250);
  uint8_t brightdepth = beatsin88( 341, 96, 224);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 300, 1500);

  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5, 9);
  uint16_t brightnesstheta16 = sPseudotime;
  d
  for ( uint16_t i = 0 ; i < numleds; i++) {
    hue16 += hueinc16;
    uint8_t hue8 = hue16 / 256;
    uint16_t h16_128 = hue16 >> 7;
    if ( h16_128 & 0x100) {
      hue8 = 255 - (h16_128 >> 1);
    } else {
      hue8 = h16_128 >> 1;
    }

    brightnesstheta16  += brightnessthetainc16;
    uint16_t b16 = sin16( brightnesstheta16  ) + 32768;

    uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
    uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
    bri8 += (255 - brightdepth);

    uint8_t index = hue8;
    //index = triwave8( index);
    index = scale8( index, 240);

    CRGB newcolor = ColorFromPalette( palette, index, bri8);

    uint16_t pixelnumber = i;
    pixelnumber = (numleds - 1) - pixelnumber;

    nblend( ledarray[pixelnumber], newcolor, 128);
  }
  }

  // Alternate rendering function just scrolls the current palette
  // across the defined LED strip.
  void palettetest( CRGB* ledarray, uint16_t numleds, const CRGBPalette16& gCurrentPalette)
  {
  static uint8_t startindex = 0;
  startindex--;
  fill_palette( ledarray, numleds, startindex, (256 / NUM_LEDS) + 1, gCurrentPalette, 255, LINEARBLEND);
  }


*/
